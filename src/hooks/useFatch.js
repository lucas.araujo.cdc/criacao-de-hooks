import { useEffect, useRef, useState } from "react"

const isObjectEqual = (obj1, obj2)=> { 
    return JSON.stringify(obj1) === JSON.stringify(obj2)
  }
  export const useFetch =  (url, options) => { 
  
    const [result, setResult] =useState(null)
    const [loading, setLoading] = useState(false)
    const [shoutLoad, setShoutLoad] = useState(false)
    const urlRef = useRef(url)
    const optionsRef = useRef(options)
  
    useEffect(() =>{
  
      let change = false;
      
      if(!isObjectEqual(url, urlRef.current)){
        urlRef.current  = url
        change=true
      }
  
      if(!isObjectEqual(options, optionsRef.current)){
        urlRef.current  = url 
        change = true
      }
  
      if(change){
        setShoutLoad((s)=> !s)
      }
    }, [url, options])
  
    useEffect(()=>{
      let wait = false; 
      setLoading(true)
  
      const controller = new AbortController()
      const signal = controller.signal 
  
      const fetchData = async ( ) => {
        console.log("EFFECT", new Date().toLocaleString())
  
        await new Promise(r => setTimeout(r, 1000))
        try {
          const response = await fetch(urlRef.current, {signal, ...optionsRef.current})
          const jsonResult = await response.json()
          if(!wait){
            setResult(jsonResult)
          }
        } catch (error) {
          console.warn(error)
        } finally{
          setLoading(false)
        }
      }
      
      fetchData()
  
      return () => {
        wait = true;
        controller.abort()
      }
  
    }, [shoutLoad])
  
    return [result, loading]
  }