import { useCallback, useEffect, useState } from "react"

export const useAsync = (asyncFunction, shoutRun)=>{

    console.log("teste")
    const [state, setState] = useState({
      result: null,
      error: null,
      status:"idle",
  
    })
  
  
    const run = useCallback(()=>{
      setState({
        result: null,
        error: null,
        status:"Pending"
      })
  
      return asyncFunction()
      .then((response)=>{
        setState({
          result: response,
          error: null,
          status:"Settled"
        })
      })
      .catch((error) => {
        setState({
          result:null,
          error: error,
          status:"Error"
        })
      })
    },[asyncFunction])
  
    useEffect(()=>{
    if(shoutRun){
      run()
    }
    },[run, shoutRun])
    return [run, state.result, state.error, state.status]
  }