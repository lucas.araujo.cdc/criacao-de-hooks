import "./App.css"
import { Suspense, lazy, useState } from "react";
import  OneComponent  from "./OneComponent";

const loadComponent = () => {
  console.log("carregando...")
  return import("./OneComponent")
}

const  LazyComponent = lazy(loadComponent)

function App() {

  const [show, setShow] = useState(false)

    return (
      <div className="App" >
        <header className="App-header" >

          <button onMouseOver={loadComponent }  onClick={()=> setShow((s)=> !s)}>{show ? 'desrenderizar' : 'renderizar'}</button>

          <Suspense fallback={<p>Carregando...</p>}> 
            {show&&<LazyComponent/>}
          </Suspense>
        </header>
      </div>
      );
}



export default App;
